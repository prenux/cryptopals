import chal3
import math
import sys

def find(filename):
    txt = open(filename)
    print("Reading : " + filename)
    char = ''
    bestScore = -(math.inf)
    plainText = []
    print("Decrypting")
    for line in txt:
        plainText.append(chal3.decrypt(line))
    plainText.sort(key=chal3.getScore, reverse=True)
    return plainText[0]

if __name__=="__main__":
    print(find(sys.argv[1]))
