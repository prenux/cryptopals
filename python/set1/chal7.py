from Crypto.Cipher import AES
import base64

def decrypt(key,enc):
    cipher = AES.new(key, AES.MODE_ECB)
    return cipher.decrypt(enc)

if __name__ == "__main__" :
    key = "YELLOW SUBMARINE"
    with open("../../../data/7.txt") as ef:
        content = ef.read()
    enc = base64.b64decode(content)
    print(decrypt(key,enc))
