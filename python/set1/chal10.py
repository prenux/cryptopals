from Crypto.Cipher import AES
from Crypto import Random
import base64
import chal9

def xorBytes(x,y):
    res = b''
    for i in range(len(x)):
        res += bytes([x[i] ^ y[i%len(y)]])
    return res

def encrypt(dec,key):
    bs = len(key)
    dec = chal9.padPKCS7(dec,bs) 
    blocks = [dec[i:i+bs] for i in range(0, len(dec), bs)]
    iv = Random.get_random_bytes(len(key))
    cipher = AES.new(key, AES.MODE_ECB)
    out = [iv,b'']
    for i in range(len(blocks)):
        enc_block = cipher.encrypt(xorBytes(blocks[i],iv))
        out[1] += enc_block
        iv = enc_block
    return out

def decrypt(enc,key,iv):
    bs = len(key)
    blocks = [enc[i:i+bs] for i in range(0, len(enc), bs)]
    cipher = AES.new(key, AES.MODE_ECB)
    dec = b''
    for i in range(len(blocks)):
        dec += xorBytes(cipher.decrypt(blocks[i]),iv)
        iv = blocks[i]
    return dec

if __name__ == "__main__" :
    print("Decryption of challenge")
    key = "YELLOW SUBMARINE"
    iv = b'\x00\x00\x00'
    with open("../../../data/10.txt") as ef:
        content = ef.read()
    enc = base64.b64decode(content)
    print(decrypt(enc,key,iv))
    print("\nencryption followed by decryption")
    key = 'abcdefghijklmnop' 
    enc = encrypt(str.encode('this is funny cause Im supposed to do something else'),key)
    print(decrypt(enc[1], key,enc[0]))
