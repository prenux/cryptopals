import itertools,binascii,chal7

def ecb_score(txt):
    score = 0
    bs = 16
    blocks = [txt[i:i+bs] for i in range(0, len(txt), bs)]
    b_pairs = itertools.combinations(blocks, 2)
    for bp in b_pairs:
        if bp[0] == bp[1] : score += 1 
    return score

if __name__ == "__main__":
    with open("../../../data/8.txt") as ef:
        line_num = 0
        for hex_line in ef:
            line = binascii.unhexlify(hex_line.strip())
            if ecb_score(line) > 0:
                print(line_num)
            line_num += 1
