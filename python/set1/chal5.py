import sys

def xorRepeat(x,key):
    ans = ""
    for i in range(len(x)):
        val1 = ord(x[i])
        val2 = ord(key[i%len(key)])
        ans += format((val1 ^ val2), '02x')
    return ans

if __name__=="__main__":
    print(xorRepeat(sys.argv[1],sys.argv[2]))
