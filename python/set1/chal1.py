import base64
import codecs
import sys

def hex2b64(data) :
    decode_hex = codecs.getdecoder("hex_codec")
    value = decode_hex(data)[0]
    encoded = base64.b64encode(value)
    return (encoded)

if __name__=="__main__":
    print(hex2b64(sys.argv[1]))
