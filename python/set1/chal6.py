import sys, string, base64

def hammingDst(x,y):
    miaw = 0
    wouf = 0
    dst = 0
    #print("Hamming: Getting differences")
    for c in x: # Get value of x
        miaw = miaw << 8
        miaw += c
    for c in y: # Get value of y
        wouf = wouf << 8
        wouf += c
    #Each different bit after been xor'd are set hot
    val = wouf ^ miaw
    #print("Hamming: Calculating distance")
    while val != 0:
    #Check LSB then >> 1 for next one
        if val%2 == 1:
            dst += 1
        val = val >> 1
    #print("Hamming: Done!")
    return dst

def findKeySizes(txt, n):
    minSize = 2 
    maxSize = 40
    keySizes = {}
    nBests = []
    n_blocks = 10
    for i in range(minSize, maxSize+1):
        if i >= len(txt):
            print("Text is shorter than : " + i + " ... skipping")
            break
        elif i*2 >= len(txt):
            print("Text too short to evaluate repetition of key with size : " + i)
            break
        dst = 0
        for j in range(n_blocks):
            for k in range(j+1,n_blocks):
                dst += hammingDst(txt[i*j:i*k],txt[i*k:i*(k+1)])/i
        # nb_of_hamming is the number of time the inner for loop is executed
        nb_of_hamming = n_blocks*(n_blocks+1)/2 - n_blocks
        # average of all hamming for current key size
        normDst = dst/(nb_of_hamming)
        keySizes[i] = normDst
    #print(keySizes)
    for i in range(n):
        bestKeySize = min(keySizes, key=keySizes.get)
        nBests.append(bestKeySize)
        del keySizes[bestKeySize]
    print(nBests)
    return nBests
        
def getBlocks(txt, size):
    blocks = [b'' for i in range(size)]
    for i in range(len(txt)):
        blocks[i%size] += txt[i:i+1]
    return blocks

#adaptation of chal3 code 
def getBlockKey(x) :
    #x is a byte object
    decrypted = []
    for e in range(256):
        decrypted.append(decode(x,e))
    plain_txt = max(decrypted, key = getScore)
    return chr(decrypted.index(plain_txt))

def decode (block,e):
    # block is a byte object and e is a int representing a char
    ans  = ""
    score = 0
    for c in block:
        r =  chr(c ^ e)
        ans += r
    return ans

def getScore(w):
    score = 0
    for c in w:
        if c[0] in (string.ascii_letters + " "):
            score += 1
            if c[0].lower() in "etaoinshrldu ":
                score += 3
    return score
#end of chal3 adaptation

#adaptation from chal5 code
def xorRepeat(txt,key):
    ans = ""
    for i in range(len(txt)):
        val1 = txt[i]
        val2 = ord(key[i%len(key)])
        ans += chr((val1 ^ val2)) 
    return ans
#end of chal5 adaptation

def getRidOf (filename, isB64):
    num_of_key_size = 3
    txt = None
    with open(filename,'rb') as fp:
        if isB64 :
            txt = base64.b64decode(fp.read())
        else:
            txt = fp.read()
    keys = ["" for i in range(num_of_key_size)]
    decrypted = ["" for i in range(num_of_key_size)]
    for i,j in enumerate(findKeySizes(txt,num_of_key_size)):
        for block in getBlocks(txt,j):
            keys[i] += getBlockKey(block)
            decrypted[i] += xorRepeat(txt,keys[i])
    return keys,decrypted


if __name__=="__main__":
    if hammingDst(str.encode("this is a test"), str.encode("wokka wokka!!!")) != 37:
            print("Error in Hamming Dst")
            exit()
    isB64 = False
    try:
        if sys.argv[2] == "b64":
            isB64 = True
        elif sys.argv[2] == "normal" :
            isB64 = False
        else:
            raise Error
    except:
        print("Usage:  python chal6.py $FILENAME [b64|normal]")
        exit()
    res=getRidOf(sys.argv[1], isB64)
    for i in range(len(res[0])):
        print("Key : " + res[0][i] + "\n\n")
        print("Text :\n" + res[1][i] + "\n\n\n\n")

