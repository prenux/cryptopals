def padPKCS7(txt, bs):
    ch = bs - (len(txt) % bs)
    return txt + bytes([ch] * ch)

if __name__ == "__main__":
    print(padPKCS7(b"YELLOW SUBMARINE", 20))
