import math
import threading
import queue
import string
import sys

def decrypt(x) :
    decrypted = []
    key=""
    for e in range(0,256):
        decrypted.append(decode(x,e))
    plain_txt = max(decrypted, key = getScore)
    return plain_txt, chr(decrypted.index(plain_txt))

def decode (x,e):
    ans  = ""
    score = 0
    for c in range(0, len(x)-1, 2):
        b = x[c] + x[c+1]
        chrInt = int(b,16) ^ e
        r =  chr(chrInt)
        ans += r
    return ans

def getScore(w):
    score = 0
    for c in w:
        if c[0] in (string.ascii_letters + " "):
            score += 1
            if c[0].lower() in "etaoinshrldu":
                score += 3
    return score

if __name__=="__main__":
   print(decrypt(sys.argv[1])) 
