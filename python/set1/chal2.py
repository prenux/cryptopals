import sys

def xorHexStr(x ,y):
    val1 = int(x, 16)
    val2 = int(y, 16)
    z = val1 ^ val2
    return hex(z)

if __name__=="__main__":
    print(xorHexStr(sys.argv[1], sys.argv[2]))
