extern crate set1;

use set1::chal1;
use set1::chal2;
use set1::chal3;
use set1::chal4;

fn main() {
    do_chal1();
    do_chal2();
    do_chal3();
    do_chal4();
}

fn do_chal1() {
    let s = "49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d";
    let res = chal1::hex_to_bytes(&s);
    println!("Challenge 1:\n\tHex decoded: {}\n\tB64 encoded: {}",chal1::bytes_to_str(&res),chal1::bytes_to_b64(&res));
}

fn do_chal2() {
    let mut a = chal1::hex_to_bytes("1c0111001f010100061a024b53535009181c");
    let b = chal1::hex_to_bytes("686974207468652062756c6c277320657965");
    let res = chal2::xor_bytes(&mut a,&b);
    println!("Challenge 2:\n\tXOR'ed: {}\n\tXOR'ed Hex: {}",
             chal1::bytes_to_str(&res.clone()),chal1::bytes_to_hex(&res));
}

fn do_chal3() {
    let msg = chal1::hex_to_bytes("1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736");
    let res = chal3::break_1b_xor(&msg,3);
    println!("Challenge 3:\n\tTop answer: {}\n\t2nd answer: {}\n\t3rd answer: {}",
             chal1::bytes_to_str(&chal2::xor_bytes(&mut msg.clone(),&vec![res[0]])),
             chal1::bytes_to_str(&chal2::xor_bytes(&mut msg.clone(),&vec![res[1]])),
             chal1::bytes_to_str(&chal2::xor_bytes(&mut msg.clone(),&vec![res[2]])));
}

fn do_chal4() {
    let res = chal4::find_xor(String::from("../../../data/4.txt"));
    println!("Challenge 4:\n\tTop answer: {}",
             chal1::bytes_to_str(&res))
}
