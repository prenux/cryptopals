extern crate rustc_serialize as serialize;

use self::serialize::base64::{self, ToBase64};
use self::serialize::hex::{FromHex,ToHex};

use std::borrow::Cow;

type Bytes = Vec<u8>;

pub fn hex_to_bytes(hexstr:&str) -> Bytes{
    hexstr.from_hex().unwrap()
}

pub fn bytes_to_hex(bytes:&Bytes) -> String{
    bytes.to_hex()
}

pub fn bytes_to_str<'a>(bytes:&'a Bytes) -> Cow<'a,str>{
    String::from_utf8_lossy(bytes)
}

pub fn bytes_to_b64(bytes:&Bytes) -> String{
    (&bytes).to_base64(base64::STANDARD)
}
