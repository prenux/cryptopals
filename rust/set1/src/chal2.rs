extern crate rustc_serialize as serialize;

type Bytes = Vec<u8>;

pub fn xor_bytes(msg:&mut Bytes,key:&Bytes) -> Bytes{
    msg.iter().zip(key.iter().cycle()).map(|(x,y)| x^y).collect()
}
