type Bytes = Vec<u8>;

use std::io::BufReader;
use std::io::prelude::*;
use std::fs::File;

use chal1::{bytes_to_str,hex_to_bytes};
use chal2::xor_bytes;
use chal3::{break_1b_xor,get_score};

pub fn find_xor(filename:String) -> Bytes{
    let f = File::open(filename).expect("File not found");
    let f = BufReader::new(f);
    let mut plaintxts = Vec::new();
    for line in f.lines() {
        match line {
            Ok(l) => {
                let ciphtxt = hex_to_bytes(&l);
                plaintxts.push(xor_bytes(&mut ciphtxt.clone(),&break_1b_xor(&ciphtxt,1)))
            },
            Err(err) => panic!("chal4::find_xor - Error in reading line: {:?}", err),
        }
    }
    match plaintxts.iter().max_by_key(|x| get_score(bytes_to_str(x).into_owned())){
        Some(t) => t.clone(),
        None => panic!("chal4::find_xor - Error in max_by_key"),
    }
}
