type Bytes = Vec<u8>;

use chal1::bytes_to_str;
use chal2::xor_bytes;

pub fn break_1b_xor(ciphtxt:&Bytes, n_res:u8) -> Vec<u8>{
    let mut scores = (0..255).map(|c| (
            c,
            get_score(
                bytes_to_str(
                    &xor_bytes(
                        &mut ciphtxt.clone(),
                        &vec![(c as u8)]
                        )
                    ).into_owned()
                )
            )
                             ).collect::<Vec<(u8,u32)>>();
    let mut tops:Vec<u8> = Vec::new();
    for _i in 0..n_res{
        let max_i =
            match scores.iter().max_by_key(|x| x.1){
                Some(t) => t.0,
                None => panic!("chal3::break_1b_xor - Error in max_by_key"),
            };
        tops.push(scores.remove(max_i as usize).0);
    }
    tops
}

pub fn get_score(plaintxt:String) -> u32{
    let top_letters = " etaoinshrdlu";
    plaintxt.chars().fold(0,|acc, c| if top_letters.contains(c) {acc + 3} else {acc})
}
